var app = angular.module('zeleb_back');

app.controller('VideoField', function ($scope, $http, $element, $attrs, $compile) {

  var $ = jQuery;

  $scope.vid = $attrs.vid;
  $scope.videos = null;
  $scope.offset = 0;
  $scope.pageSize = 10;


  var $popup = $('<div ng-include="\'field-jwplayer-popup.html\'"></div>');
  $compile($popup)($scope);
  $('body').append($popup);

  var opts = {
    lines: 13, // The number of lines to draw
    length: 20, // The length of each line
    width: 10, // The line thickness
    radius: 30, // The radius of the inner circle
    corners: 1, // Corner roundness (0..1)
    rotate: 0, // The rotation offset
    direction: 1, // 1: clockwise, -1: counterclockwise
    color: '#000', // #rgb or #rrggbb or array of colors
    speed: 1, // Rounds per second
    trail: 60, // Afterglow percentage
    shadow: false, // Whether to render a shadow
    hwaccel: false, // Whether to use hardware acceleration
    className: 'spinner', // The CSS class to assign to the spinner
    zIndex: 2e9, // The z-index (defaults to 2000000000)
    top: '50%', // Top position relative to parent
    left: '50%' // Left position relative to parent
  };

  $scope.numberOfPages = function(){
    if($scope.videos){
      return Math.ceil($scope.videos.length/$scope.pageSize);
    }
    else{
      return 0;
    }
  }

  $('.current-video img').error(function() {
    setTimeout(function(){

        var uri = new URI($('.current-video img').attr('src'));

        var query = {
          'random': Math.random()
        };
        uri.query(query);
      if ($('.current-video img').attr('src') != "http://content.jwplatform.com/thumbs/-120.jpg") {
        $('.current-video img').attr('src', uri.toString());
      }
    }, 5000);
  });

  $scope.createUploadForm = function() {
    // Have the server perform an API call to create the video.
    var data = {};
    var url  = '/api/1.0/jw_video/create';

    console.log(url);

    jQuery.get(url, data, function (data) {
      // Attach a BotrUpload instance to the form.
      var upload = new BotrUpload(data.link, data.session_id);

      upload.onStart = function(size, redirect) {
        console.log("empezamos la subida");
        window.ajaxCalls++;
        if (window.ajaxCalls == 0) {
          angular.element('.loaderSpinner').hide();
        }
      };

      upload.onCompleted = function(size, redirect) {

        console.log(data.media.key);

        window.ajaxCalls--;
        angular.element('.loaderSpinner').hide();

        var body = {
          'token' : data.media.key,
          'title' : $('#uploadForm #uploadTitle').val()
        };

        $http.post('/api/1.0/video/create', body).success(function (data) {
          var video = {
            'token' : body['token'],
            'title' : body['title'],
            'vid' : data['vid']
          }
          $scope.setVideo(video);
          $scope.closePicker();
          $('#uploadForm #uploadFile').replaceWith($('#uploadForm #uploadFile').val('').clone(true));
          $('#uploadForm #uploadTitle').val("");
        });

        //$scope.$apply();

      };
      upload.useForm(jQuery("#uploadFile").get(0));
      jQuery("body").append(upload.getIframe());
      upload.pollInterval = 1000;

      // During upload, we update both the progress div and the text below it.
      upload.onProgress = function (bytes, total) {
        // Round to one decimal
        var pct = Math.floor(bytes * 1000 / total) / 10;
        jQuery("#uploadProgress").animate({'width': pct + '%'}, 400);
        jQuery("#uploadText").html('Uploading (' + pct + '%) ...');
      };
    }, 'json');
  }

  $scope.getVideo = function () {

    var uri = new URI( '/api/1.0/video');

    var query = {
      'token': $scope.token
    };
    uri.query(query);

    $http.get(uri.toString()).success(function (data) {
      $scope.currentVideo = data[0];
    });
  };

  $scope.loadVideos = function () {
    var uri = new URI('/api/1.0/video/index');

    var query = {
      'offset': $scope.offset,
      'count': $scope.pageSize
    };

    if($('#filterTitle').val() != "") {
      query['title'] = $('#filterTitle').val();
    }
    uri.query(query);

    $http.get(uri.toString()).success(function (data) {
      console.log(data);
      $scope.videos = data;
    });
  };

  $scope.setVideo = function (video) {
    console.log(video);
    $scope.token = video.token;
    $scope.title = video.title;
    $scope.currentVideo = video;
    $scope.closePicker();
   //$scope.checkNoFeed();
  };

  $scope.removeVideo = function () {
    $scope.vid = "";
    $scope.currentVideo = undefined;
    //$scope.uncheckNoFeed();
  };

  $scope.showPicker = function () {
    jQuery('.wrapper-popup').fadeIn();
    $scope.createUploadForm();
    $scope.loadVideos();
    jQuery('.loaderSpinner').spin();
  };

  $scope.closePicker = function () {
    jQuery('.wrapper-popup').fadeOut();
  };

  $scope.previosPage = function () {
    $scope.offset -= $scope.pageSize;
    $scope.loadVideos();
  };

  $scope.nextPage = function () {
    $scope.offset += $scope.pageSize;
    $scope.loadVideos();
  };

  $scope.filterVideos = function () {
    $scope.loadVideos();
  };

  $scope.uncheckNoFeed = function() {
    angular.element('#edit-field-news-feed-msn-video-und').prop('checked', false);
  }

  $scope.checkNoFeed = function() {
    angular.element('#edit-field-news-feed-msn-video-und').prop('checked', true);
  }

  if ($scope.token) {
    $scope.getVideo();
  }
});
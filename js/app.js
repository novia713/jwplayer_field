window.ajaxCalls = 0;

angular.module('SharedServices', [])
    .config(function($interpolateProvider){
      $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
    })
  .config(function ($httpProvider) {
    $httpProvider.responseInterceptors.push('myHttpInterceptor');
    var spinnerFunction = function (data, headersGetter) {
      window.ajaxCalls++;
      angular.element('.loaderSpinner').show();
      return data;
    };
    $httpProvider.defaults.transformRequest.push(spinnerFunction);
  })
  // register the interceptor as a service, intercepts ALL angular ajax http calls
  .factory('myHttpInterceptor', function ($q, $window) {
    return function (promise) {
      return promise.then(function (response) {
        window.ajaxCalls--;
        if (window.ajaxCalls == 0) {
          angular.element('.loaderSpinner').hide();
        }
        return response;

      }, function (response) {
        window.ajaxCalls--;
        if (window.ajaxCalls == 0) {
          angular.element('.loaderSpinner').hide();
        }
        return $q.reject(response);
      });
    };
  });

var app = angular.module('zeleb_back', ['SharedServices']);
<?php
namespace Drupal\jwplayer_field\Plugin\Field\FieldType;
/**
 * Provides a field type of JWPlayer videos that are uploaded to JWPlatform
 *
 * @FieldType(
 *   id = "jwplayer_upload",
 *   label = @Translation("Video para JWPlayer"),
 *   module = "jwplayer_field",
 *   description = @Translation("Permite la subida de un video a la plataforma JWPlayer."),
 *   default_widget = "jwplayer_field_jwplayer_fielddefault",
 *   default_formatter = "jwplayer_field_formatter"
 * )
 */

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

class Jwplayer_field_Item extends FieldItemBase implements FieldItemInterface
{

    /**
     * {@inheritdoc}
     */
    public static function schema(FieldStorageDefinitionInterface $field_definition)
    {
        return array(
            'columns' => array(
                'value' => array(
                    'type' => 'varchar',
                    'length' => 128,
                    'not null' => FALSE,
                ),
            ),

        );
    }

    /**
     * {@inheritdoc}
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition)
    {

        $properties['value'] = DataDefinition::create('string')
            ->setLabel(t('Valor'))
            ->setDescription(t('valor'));

        return $properties;
    }

    /**
     * {@inheritdoc}
     */
    public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
      $random = new Random();
      $values['value'] = $random->word(mt_rand(1, 128));
      return $values;
    }

    /**
     * {@inheritdoc}
     */
    public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
      $elements = [];

      $elements['max_length'] = [
        '#type' => 'number',
        '#title' => t('Maximum length'),
        '#default_value' => 128,
        '#required' => FALSE,
        '#description' => t('The maximum length of the field in characters.'),
        '#min' => 1,
        '#disabled' => $has_data,
      ];

      return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty() {
      $value = $this->get('value')->getValue();
      return $value === NULL || $value === '';
    }

}

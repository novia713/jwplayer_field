<?php
/**
 * @file
 * Contains \Drupal\jwplayer_field\Plugin\Field\FieldWidget\TextWidget.
 */

namespace Drupal\jwplayer_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_example_text' widget.
 *
 * @FieldWidget(
 *   id = "jwplayer_field_jwplayer_fielddefault",
 *   module = "jwplayer_field",
 *   label = @Translation("Selector de JWPlayer"),
 *   field_types = {
 *     "jwplayer_upload"
 *   }
 * )
 */
class Jwplayer_fieldDefaultWidget extends WidgetBase
{

    /**
     * {@inheritdoc}
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state)
    {
        $value = isset($items[$delta]->value) ? $items[$delta]->value : '';

        $libraries = array(
            'jwplayer_field/jwplayer_field.angularjs',
            'jwplayer_field/jwplayer_field.uri',
            'jwplayer_field/jwplayer_field.botr_uploader',
            'jwplayer_field/jwplayer_field.spin',
            'jwplayer_field/jwplayer_field.jquery_spin',
            'jwplayer_field/jwplayer_field.angular_app',
            'jwplayer_field/jwplayer_field.angular_controller',
            'jwplayer_field/jwplayer_field.angular_directive',
        );

        $element += array(
            '#type' => 'textfield',
            '#theme' => 'jwplayer_form',
            '#size' => 20,
            '#maxlength' => 128,
            '#title' => t('Selecciona el video de la noticia:aaaa'),
            '#default_value' => $value,
            '#attached' => array(
                'library' => $libraries,
            ),
            //'variables' => array('data-endpoint' => 'cccc'),
             '#attributes' => array(
                 'ng-model' => 'token',
                 'token' => $value,
                 'class' => array('element-hidden'),
             ),
            '#cache' => [
              'max-age' => 0
            ]
        );

        return array('value' => $element);
    }

}

<?php

/**
 * @file
 * Contains \Drupal\jwplayer_field\Plugin\Field\FieldFormatter\Jwplayer_fieldDefaultFormatter.
 */

namespace Drupal\jwplayer_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'jwplayer_field_text' formatter.
 *
 * @FieldFormatter(
 *   id = "jwplayer_field_formatter",
 *   label = @Translation("ID del video de JWPlayer"),
 *   field_types = {
 *     "jwplayer_upload"
 *   }
 * )
 */
class Jwplayer_fieldDefaultFormatter extends FormatterBase {

    /**
     * {@inheritdoc}
     */
    public function settingsSummary() {
        $summary = array();
        $settings = $this->getSettings();

        $summary[] = t('Displays the video id string.');

        return $summary;
    }

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {
        $element = array();

        foreach ($items as $delta => $item) {
            // Render each element as markup.
            $element[$delta] = array(
                '#type' => 'markup',
                '#markup' => $item->value,
            );
        }

        return $element;
    }
}

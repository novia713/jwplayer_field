<?php

/**
 * Created by PhpStorm.
 * User: abel
 * Date: 16/8/16
 * Time: 11:42
 */

namespace Drupal\jwplayer_field\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Query\SelectInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

require_once(drupal_get_path('module', 'jwplayer_field').'/includes/api.php');

class Jwplayer_fieldController extends ControllerBase {

    /**
     * A simple page to explain to the developer what to do.
     */
    public function description() {
        return array(
            '#markup' => t(
                "The Field Example provides a field composed of an HTML RGB value, like #ff00ff. To use it, add the field to a content type."),
        );
    }

    public function jwplayer_video_get(){
        $this->wp_ws_validate_methods(array('GET'));

        $video_key = $_GET['video_key'];

        $botr_api = new \BotrAPI('26aCBjgs', 'GSsOABAYnDVL6j2SaSARcPZO');
        $response = $botr_api->call('/videos/show', array('video_key'=>$video_key));

        $this->wp_ws_response(200, $response['video']['title']);
        return new JsonResponse($response['video']['title']);
    }

    public function jwplayer_video_index(){
        $this->wp_ws_validate_methods(array('GET'));

        $offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
        $count = isset($_GET['count']) ? $_GET['count'] : 0;

        $query = \Drupal::database()->select('node_field_data', 'nfd');
        $query->fields('nfd', ['nid', 'title']);
        $query->condition('nfd.type', 'zeleb_news');
        //$query->range(0, 1);


        $botr_api = new \BotrAPI('26aCBjgs', 'GSsOABAYnDVL6j2SaSARcPZO');

        $params = array('order_by'=>'date:desc');

        if (isset($_GET['title'])){
            $params['text'] = $_GET['title'];
        }

        if (isset($_GET['count'])){
            $params['result_limit'] = $_GET['count'];
        }

        if (isset($_GET['offset'])){
            $params['result_offset'] = $_GET['offset'];
        }

        $botr_response = $botr_api->call("/videos/list", $params);//,array('text'=>($search),'total_limit'=>20));
        if ($botr_response['status'] == "error") { die(print_r($botr_response)); }
            $message = sizeof($botr_response['videos']) . " search result(s):";

        $videos = array();
        foreach ($botr_response['videos'] as $video){
            $videos[] = array('token' => $video['key'], 'title' => $video['title'] . " " . $video['description']);
        }

        $this->wp_ws_response(200, $videos);
        return new JsonResponse($videos);
    }

    public function jwplayer_jw_video_create(){
        $this->wp_ws_validate_methods(array('GET'));

        // Do the API call and send the result back to the client.
        $botr_api = new \BotrAPI('26aCBjgs', 'GSsOABAYnDVL6j2SaSARcPZO');
        $response = $botr_api->call('/videos/create');

        if ($response['status'] == 'ok'){
            $this->wp_ws_response(200, $response );
            return new JsonResponse($response);
        }
        else {
            $this->wp_ws_response(400);
        }
    }

    public function jwplayer_video_create(){
        $this->wp_ws_validate_methods(array('POST'));

        $params = array(
            'token' => $this->wp_ws_get_input_data('token'),
            'title' => $this->wp_ws_get_input_data('title', false),
        );

        //$vid = $params['token'];


        if ($params['token']){
            return new JsonResponse($params['token']);
        }
        else {
            $this->wp_ws_response(400);
        }
    }

    private function wp_ws_validate_methods($methods)
    {
        if (!in_array($_SERVER['REQUEST_METHOD'], $methods)) {
            http_response_code(405);
            print $_SERVER['REQUEST_METHOD'];
            drupal_exit();
        }
        return $_SERVER['REQUEST_METHOD'];
    }

    private function wp_ws_get_input_data($name, $required = TRUE)
    {
        $data = json_decode(file_get_contents('php://input'), true);
        if (!isset($data[$name]) && $required) {
            http_response_code(400);
            drupal_exit();
        }
        if (!isset($data[$name]) && !$required) {
            return null;
        }
        return $data[$name];
    }

    private function wp_ws_response($code, $data = null)
    {
        http_response_code($code);
        return new JsonResponse($data);
    }
}